from django.test import TestCase, Client
from django.urls import resolve
import unittest
import time
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options
from .views import index,register

class Story9UnitTest(TestCase):
    def test_url_is_exist(self):
        response = Client().get('/')
        self.assertEqual(response.status_code, 200)
    def test_using_index(self):
        found = resolve('/')
        self.assertEqual(found.func, index)
    def test_using_index2(self):
        found = resolve('/signup/')
        self.assertEqual(found.func, register)
    def test_page_template(self):
        response = Client().get('/')
        self.assertTemplateUsed(response,'landing.html')
    def test_page_template2(self):
        response = Client().get('/signup/')
        self.assertTemplateUsed(response,'registration/signup.html')

