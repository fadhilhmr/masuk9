from django.contrib import admin
from django.urls import path
from .views import index,register

urlpatterns = [
    path('', index),
    path('signup/',register, name='signup'),
]
